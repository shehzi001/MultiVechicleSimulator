package MultiVechicleUICommTest;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import MultiVechicleAMIImpl.MultiVechicleNode;
import MultiVechicleAMIImpl.MultiVechiclePublisherImpl;
import MultiVechicleAMIImpl.MultiVechicleSubscriberImpl;

public class MultiVechicleUICommSubTest extends MultiVechicleSubscriberImpl {

	MultiVechicleNode mv_node, mv_node1;

	static MultiVechiclePublisherImpl robot_command_pub;

	@Override
	public void robot_move_command(String msg) {
		System.out.println("robot move command " + mv_node.node_name + " : " + msg);
	}

	@Override
	public void robot_select_command(int msg) {
		System.out.println("robot select command " + mv_node1.node_name + " : " + msg);
	}

	@Override
	public void robot_spawn_command(int msg) {
		System.out.println(mv_node.node_name + " : " + msg);
	}

	public MultiVechicleUICommSubTest() {
		mv_node = new MultiVechicleNode();
		mv_node.init_node("CommSubscriberTest");
		mv_node.Subscribe("RobotMoveCommand", this);

		mv_node1 = new MultiVechicleNode();
		mv_node1.init_node("CommSubscriberTest1");
		mv_node1.Subscribe("RobotSelectCommand", this);

		// mv_node.Subscribe("RobotSelectCommand", this);

		mv_node.Start();
		mv_node1.Start();

		if (mv_node.is_node_running()) {
			System.out.println(mv_node.node_name + " : Node is running.");
			// mv_node.Spin();
			// mv_node1.Spin();
		} else {
			System.out.println(mv_node.node_name + " : Node is unable to start");
			System.out.println(mv_node.node_name + " : Program Exiting.");
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try {
			for (;;) {
				String msg = br.readLine();
			}
		} catch (Exception e) {
			System.out.println(mv_node.node_name + " : Program Exiting.");
		}
	}

	public static void main(String args[]) {

		MultiVechicleUICommSubTest mv = new MultiVechicleUICommSubTest();
	}
}
