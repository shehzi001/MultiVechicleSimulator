package MultiVechicleUICommTest;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import MultiVechicleAMIImpl.MultiVechicleNode;
import MultiVechicleAMIImpl.MultiVechiclePublisherImpl;

public class MultiVechicleUICommPubTest {

	static MultiVechicleNode mv_node, mv_node1;

	static MultiVechiclePublisherImpl robot_command_pub, robot_select_pub;

	public MultiVechicleUICommPubTest() {
		mv_node = new MultiVechicleNode();
		mv_node.init_node("CommPubscriberTest");
		robot_command_pub = mv_node.Advertise("RobotMoveCommand");

		// mv_node1 = new MultiVechicleNode();
		// mv_node1.init_node("CommPubscriberTest1");
		robot_select_pub = mv_node.Advertise("RobotSelectCommand");

		mv_node.Start();
		// mv_node1.Start();
		if (mv_node.is_node_running()) {
			System.out.println(mv_node.node_name + " : Node is running.");
			// mv_node.Spin();
		} else {
			System.out.println(mv_node.node_name + " : Node is unable to start");
			System.out.println(mv_node.node_name + " : Program Exiting.");
		}
	}

	public static void main(String args[]) {

		MultiVechicleUICommPubTest mv = new MultiVechicleUICommPubTest();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try {
			for (;;) {
				System.out.print("mv_node.node_name > ");
				String msg = br.readLine();
				robot_command_pub.robot_move_command(msg);
				robot_select_pub.robot_select_command(10);
			}
		} catch (Exception e) {
			System.out.println(mv_node.node_name + " : Program Exiting.");
			// e.printStackTrace();
		}
	}
}
