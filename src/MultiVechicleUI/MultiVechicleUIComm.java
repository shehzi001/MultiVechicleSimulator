/**
 * Author : Shehzad Ahmed
 * The class uses MultiVechicleUIComm to provide asynchronous communication
 * interface with the MultiVechicle Simulator(MVS). It used simple publisher
 * and subscriber model wrapper to develop a communication channel using CORBRA
 * communication layer.
 */

package MultiVechicleUI;

import MultiVechicleAMIImpl.MultiVechicleNode;
import MultiVechicleAMIImpl.MultiVechiclePublisherImpl;
import MultiVechicleAMIImpl.MultiVechicleSubscriberImpl;

public class MultiVechicleUIComm extends MultiVechicleSubscriberImpl {

	MultiVechicleNode mv_node;
	MultiVechiclePublisherImpl robot_command_pub;
	MultiVechiclePublisherImpl robot_spawn_pub;
	MultiVechiclePublisherImpl robot_select_pub;

	@Override
	public void message(String msg) {
		System.out.println(mv_node.node_name + " : " + msg);
	}
	
	/**
	 * It initializes a communication channel using init_node
	 * and then it advertises different topics to publish commands.
	 */
	public MultiVechicleUIComm() {
		mv_node = new MultiVechicleNode();
		mv_node.init_node("MultiVechicleUI");
		robot_command_pub = mv_node.Advertise("RobotMoveCommand");
		robot_spawn_pub = mv_node.Advertise("RobotSpawnCommand");
		robot_select_pub = mv_node.Advertise("RobotSelectCommand");
		// mv_node.Subscribe("RobotFeedback", this);
		mv_node.Start();

		if (mv_node.is_node_running()) {
			System.out.println(mv_node.node_name + " : Node is running.");
		} else {
			System.out.println(mv_node.node_name + " : Node is unable to start.");
		}
	}
}
