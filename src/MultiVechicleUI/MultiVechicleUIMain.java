/**
 * Author : Shehzad Ahmed
 * This package provides a simple user interface to send commands to MultiVechicle Simulator(MVS).
 *
 */

package MultiVechicleUI;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MultiVechicleUIMain {
	private MultiVechicleUI mvs_ui_frame;
	private MultiVechicleUIComm mv_ui_comm;
	private static int ui_frame_width = 500;
	private static int ui_frame_height = 100;
	
	/**
	 * It initializes components to launch GUI for UI.
	 */
	public MultiVechicleUIMain() {
		mv_ui_comm = new MultiVechicleUIComm();

		mvs_ui_frame = new MultiVechicleUI(mv_ui_comm);
		mvs_ui_frame.setSize(ui_frame_width, ui_frame_height);
		mvs_ui_frame.setResizable(true);
		mvs_ui_frame.setVisible(true);
		mvs_ui_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mvs_ui_frame.setTitle("MultiRobotSimulator V2");
		mvs_ui_frame.setLocationRelativeTo(null);
		mvs_ui_frame.setAlwaysOnTop(true);
	}

	public static void main(String args[]) {

		MultiVechicleUIMain mvui_main = new MultiVechicleUIMain();
		mvui_main.getInput();

	}
	
	/**
	 * It provides command line interface compared to UI to enter commands.
	 */
	public void getInput() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try {
			for (;;) {
				System.out.print("Enter S to spawn robots or A to choose robot for move >");

				System.out.print("  ");
				String msg = br.readLine();
				if (msg.equals("S")) {
					System.out.print("Enter number of robots to spawn ");
					System.out.print(" > ");
					System.out.print("  ");
					msg = br.readLine();
					mv_ui_comm.robot_spawn_pub.robot_spawn_command(Integer.parseInt(msg));
				} else if (msg.equals("A")) {
					System.out.print("Enter number of robots to move ");
					System.out.print(" > ");
					msg = br.readLine();
					mv_ui_comm.robot_select_pub.robot_select_command(Integer.parseInt(msg));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
