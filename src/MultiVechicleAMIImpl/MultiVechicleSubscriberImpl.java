package MultiVechicleAMIImpl;

import MultiVechicleAMI.SubscriberInterfacePOA;

public class MultiVechicleSubscriberImpl extends SubscriberInterfacePOA {
	public void message(String msg) {
		System.out.println("Message from server : " + msg);
	}

	@Override
	public void message_double(double msg) {
		// System.out.println("Message from double server : " + (msg + 20.0));
	}

	@Override
	public void message_float(float msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_int(int msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_boolean(boolean msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_array_string(String[] msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_array_double(double[] msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_array_float(float[] msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_array_int(int[] msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void message_array_boolean(boolean[] msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void robot_move_command(String msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void robot_select_command(int msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void robot_spawn_command(int msg) {
		// TODO Auto-generated method stub

	}
}