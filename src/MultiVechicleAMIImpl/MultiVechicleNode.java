package MultiVechicleAMIImpl;

import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import MultiVechicleAMI.PublisherInterface;
import MultiVechicleAMI.PublisherInterfaceHelper;
import MultiVechicleAMI.SubscriberInterface;
import MultiVechicleAMI.SubscriberInterfaceHelper;

public class MultiVechicleNode {

	public String node_name;
	private String server_address = "localhost";
	private String server_port = "1050";

	public ORB server_handle = null;
	public POA node_handle = null;

	private boolean debug = false;

	private boolean node_status = false;

	public MultiVechicleNode() {
	}

	public MultiVechicleNode(String server_address, String port) {
		this.server_address = server_address;
		this.server_port = port;
	}

	public void init_node(String node_name) {
		this.node_name = node_name;

		String[] args = { "" };

		try {
			// initialize orb
			Properties props = System.getProperties();
			props.put("org.omg.CORBA.ORBInitialPort", server_port);

			// address of the host on which you are running the server
			props.put("org.omg.CORBA.ORBInitialHost", server_address);
			server_handle = ORB.init(args, props);

			// System.out.println("ORB is initialized");

			// Instantiate Servant and create reference
			node_handle = POAHelper.narrow(server_handle.resolve_initial_references("RootPOA"));

			node_status = true;

		} catch (Exception e) {

			node_status = false;
			System.out.println(node_name + " : Node Creation failed.");
			if (debug)
				e.printStackTrace();
		}

	}

	public MultiVechiclePublisherImpl Advertise(String topic_name) {

		if (!node_status) {
			System.out.println(node_name + " : Topic " + topic_name + "  advertisement is failed");
			System.out.println(node_name + " : Node is not initialized.");
			// System.out.println("Reason: Initialize node by calling
			// init_node(...).");

			return null;
		}

		MultiVechiclePublisherImpl msImpl = null;

		try {
			msImpl = new MultiVechiclePublisherImpl();
			node_handle.activate_object(msImpl);
			PublisherInterface msRef = PublisherInterfaceHelper.narrow(node_handle.servant_to_reference(msImpl));

			// Bind reference with NameService
			NamingContext namingContext = NamingContextHelper
					.narrow(server_handle.resolve_initial_references("NameService"));
			// System.out.println("Resolved NameService");

			NameComponent[] nc = { new NameComponent(topic_name, "") };
			namingContext.rebind(nc, msRef);

			// REMOVE THE NEXT LINE FOR THE SIMPLER EXAMPLE
			// msImpl.startReadThread();

			node_status = true;

			return msImpl;

		} catch (Exception e) {

			node_status = false;

			System.out.println(node_name + " : Exception, topic " + topic_name + "  advertisement is failed.");
			if (debug)
				e.printStackTrace();
		}

		return null;
	}

	public void Subscribe(String topic_name, MultiVechicleSubscriberImpl listener) {

		if (!node_status) {
			System.out.println(node_name + " :Topic " + topic_name + "  subscription is failed");

			return;
		}

		try {
			node_handle.activate_object(listener);
			SubscriberInterface ref = SubscriberInterfaceHelper.narrow(node_handle.servant_to_reference(listener));

			// Resolve MessageServer
			String topic_address = server_address + ":" + server_port + "#" + topic_name;
			String topic_address_full = "corbaname:iiop:1.2@" + topic_address;

			PublisherInterface msgServer = PublisherInterfaceHelper
					.narrow(server_handle.string_to_object(topic_address_full));

			// Register listener reference (callback object) with
			// MessageServer
			msgServer.register(ref);
			// System.out.println("Listener registered with the
			// Server");

			System.out.println(node_name + " :Topic " + topic_name + "  subscription is success");

		} catch (Exception e) {
			System.out.println(node_name + " : Exception caught, topic " + topic_name + " subscription is failed.");
			node_status = false;
			if (debug)
				e.printStackTrace();
		}
		// Thread t = new Thread(new OneShotTask(topic_name, listener));
		// t.start();
	}

	public void Start() {

		if (!node_status) {
			System.out.println(node_name + " : Node Start failed.");
			return;
		}

		try {
			// Activate rootpoa
			node_handle.the_POAManager().activate();

			// Start readthread and wait for incoming requests
			// System.out.println(node_name + " node is ready and running
			// ....");

		} catch (Exception e) {
			System.out.println(node_name + " : Node Start failed.");
			node_status = false;
			if (debug)
				e.printStackTrace();
		}

	}

	public void Spin() {
		server_handle.run();
	}

	public void Stop() {
		server_handle.shutdown(true);
	}

	public boolean is_node_running() {
		return node_status;
	}

	protected void finalize() throws Throwable {
		Stop();
		System.out.println(node_name + ": Node is killed.");
		super.finalize();
	}
}
