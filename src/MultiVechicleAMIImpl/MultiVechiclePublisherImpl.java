package MultiVechicleAMIImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Vector;

import MultiVechicleAMI.PublisherInterfacePOA;
import MultiVechicleAMI.SubscriberInterface;

public class MultiVechiclePublisherImpl extends PublisherInterfacePOA {

	private Vector<SubscriberInterface> clients = new Vector<SubscriberInterface>();
	// private ReadThread rt = null;

	public MultiVechiclePublisherImpl() {
		// rt = new ReadThread(this);
	}

	public void register(SubscriberInterface lt) {
		clients.add(lt);
	}

	/*
	 * public void startReadThread() { rt.start(); }
	 */

	public void message(String msg) {
		Iterator<SubscriberInterface> it = clients.iterator();
		while (it.hasNext()) {
			SubscriberInterface lt = (SubscriberInterface) it.next();
			lt.message(msg);
		}
	}

	public void message_double(double msg) {
		Iterator<SubscriberInterface> it = clients.iterator();
		while (it.hasNext()) {
			SubscriberInterface lt = (SubscriberInterface) it.next();
			lt.message_double(msg);
		}
	}

	public void robot_move_command(String msg) {
		Iterator<SubscriberInterface> it = clients.iterator();
		while (it.hasNext()) {
			SubscriberInterface lt = (SubscriberInterface) it.next();
			lt.robot_move_command(msg);
		}

	}

	public void robot_select_command(int msg) {
		Iterator<SubscriberInterface> it = clients.iterator();
		while (it.hasNext()) {
			SubscriberInterface lt = (SubscriberInterface) it.next();
			lt.robot_select_command(msg);
		}

	}

	public void robot_spawn_command(int msg) {
		Iterator<SubscriberInterface> it = clients.iterator();
		while (it.hasNext()) {
			SubscriberInterface lt = (SubscriberInterface) it.next();
			lt.robot_spawn_command(msg);
		}

	}

}

// EXCLUDE THIS CLASS FOR THE SIMPLER EXAMPLE
class ReadThread extends Thread {

	MultiVechiclePublisherImpl msImpl = null;

	public ReadThread(MultiVechiclePublisherImpl msImpl) {
		this.msImpl = msImpl;
	}

	public void run() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try {
			for (;;) {
				System.out.print("message > ");
				String msg = br.readLine();
				msImpl.message(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
