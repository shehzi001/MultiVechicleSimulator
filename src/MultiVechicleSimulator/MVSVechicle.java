/**
 * Author : Shehzad Ahmed
 * This Class provides representation of the one vehicle. The position of the 
 *  the vehicle is represented using (x,y,a) and shape is represented
 *  by the rectangle(w,h). This class also provides functionality to compute
 *  motion of the vehicle. 
 */


package MultiVechicleSimulator;

public class MVSVechicle {

	private double x, y, a; // x,y and angle
	private int w, h; // width and height
	public boolean vehicle_active;
	public int vechicle_id;

	/**
	 *  Constructor
	 * @param x x-coordinate of the vehicle.
	 * @param y y-coordinate of the vehicle.
	 * @param a angle of the vehicle(rads).
	 * @param w rectangular width of the vehicle.
	 * @param h rectangular height of the vehicle.
	 */
	public MVSVechicle(double x, double y, double a, int w, int h) {

		this.x = x;
		this.y = y;
		this.a = a;
		this.w = w;
		this.h = h;
		this.vechicle_id = -1;
		this.vehicle_active = false;
	}
	
	/**
	 * @return returns current x position of the vehicle.
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * @return returns current y position of the vehicle.
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * @return returns current angle of the vehicle.
	 */
	public double getA() {
		return a;
	}
	
	/**
	 * @return returns width of the vehicle rectangular shape.
	 */
	public int getW() {
		return w;
	}
	
	/**
	 * @return returns height of the vehicle rectangular shape.
	 */
	public int getH() {
		return h;
	}

	/**
	 *  Stores the angle of the vehicle.
	 * @param aa Angle in degrees
	 */
	public void setA(int aa) {

		a = Math.toRadians(aa);
	}

	/**
	 *  compute motion for the forward motion.
	 */
	public void moveForward() {
		x += Math.cos(a);
		y += Math.sin(a);
	}

	/**
	 *  compute motion for the backward motion.
	 */
	public void moveBackward() {
		x -= Math.cos(a);
		y -= Math.sin(a);
	}

}