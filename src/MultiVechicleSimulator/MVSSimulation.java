/**
 * Author : Shehzad Ahmed
 * This Class implements functionality of the Simulator GUI.  Simulation is continuously
 * updated using shared resources from the motion controller. It also adds logistics terminals
 * as rectangular boxes. Based on the status of vehicle's activation, it represented by using 
 * green (active state) and white colors(idle state).
 */

package MultiVechicleSimulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class MVSSimulation extends JPanel {

	private static final long serialVersionUID = 1L;

	ArrayList<MVSVechicle> vechicles;
	ArrayList<Rectangle> logistic_terminal_objects;

	public static int window_w = 800;
	public static int window_h = 600;

	/**
	 *  Constructor
	 */
	public MVSSimulation() {
		init();
		setFocusable(true);
		setBackground(new Color(0, 50, 100));
		setDoubleBuffered(true);
		setFocusable(true);

	}

	/**
	 *  Initializes vehicles and logistic terminals buffer.
	 */
	private void init() {

		vechicles = new ArrayList<MVSVechicle>();
		
		logistic_terminal_objects = new ArrayList<Rectangle>();
		logistic_terminal_objects.add(new Rectangle(30, (int)(0.2*window_h), 50, 400));
		logistic_terminal_objects.add(new Rectangle((int)(0.3*window_w), (int)(0.9*window_h), 400 , 50));
		logistic_terminal_objects.add(new Rectangle((int)(0.3*window_w), 10, 400 , 50));
		logistic_terminal_objects.add(new Rectangle(window_w-100, (int)(0.2*window_h), 50, 400));

	}
	
	/**
	 *  This method is synchronized to prevent simultaneous access
	 *  to vehicles buffer from external threads.
	 */
	public synchronized int spawn_robots(MVSVechicle v) {
		vechicles.add(v);
		return vechicles.indexOf(v);
	}
	
	/**
	 *  This method is also synchronized to prevent simultaneous access
	 *  from different threads.
	 */
	public synchronized boolean updateSimulation(MVSVechicle v) {

		if (v.vechicle_id >= 0 && v.vechicle_id < vechicles.size()) {
			vechicles.set(v.vechicle_id, v);
			return true;
		}
		
		return false;
	}
	
	/**
	 * The methods call repaint method to update simulation.
	 * It is synchronized to stop multiple threads calling 
	 * at the same time.
	 */
	public synchronized void updateGUI() {
		repaint();
	}

	/**
	 *  Paint method is re-implement to customize simulated 
	 *  graphics the functionality. 
	 */
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		for (int iter = 0; iter < vechicles.size(); iter++) {

			MVSVechicle Vechicle = vechicles.get(iter);

			AffineTransform old = g2d.getTransform();

			if (vechicles.get(iter).vehicle_active)
				g2d.setColor(Color.GREEN);
			else
				g2d.setColor(Color.WHITE);

			// rotating the Vechicle, rotation point is the middle of the square
			g2d.rotate(Vechicle.getA(), Vechicle.getX() + Vechicle.getW() / 2, Vechicle.getY() + Vechicle.getH() / 2);
			
			// drawing the square
			g2d.drawRect((int) Vechicle.getX(), (int) Vechicle.getY(), Vechicle.getW(), Vechicle.getH());
			g2d.drawString(Integer.toString(iter+1), (int) Vechicle.getX() + Vechicle.getW()/2, (int) Vechicle.getY() + Vechicle.getH()/2);
			
			// draw the "head"
			g2d.fillRect((int) Vechicle.getX() + Vechicle.getW(), (int) Vechicle.getY() + 3, 10, 20);

			// in case you have other things to rotate
			g2d.setTransform(old);
		}
		
		// draw AGVs parking
		g2d.drawRect(30, (int)(0.2*window_h), 50, 400);
		g2d.drawString("AGVs terminals", 30, (int)(0.18*window_h));
		
		// draw storage section
		g2d.drawRect((int)(0.3*window_w), (int)(0.9*window_h), 400 , 50);
		g2d.drawString("Storage terminals", (int)(0.3*window_w)+200, (int)(0.9*window_h) + 25);
		
		// draw docking terminal section
		g2d.drawRect((int)(0.3*window_w), 10, 400 , 50);
		g2d.drawString("Docking terminal", (int)(0.3*window_w) + 200, 35);
		
		// draw AGVs workstation
		g2d.drawRect(window_w-100, (int)(0.2*window_h), 50, 400);
		g2d.drawString("AGVs workstation", window_w-150, (int)(0.18*window_h));

		// done with g2d, dispose it
		g2d.dispose();
	}
}