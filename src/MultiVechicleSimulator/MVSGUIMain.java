/**
 * Author : Shehzad Ahmed
 * This package provides a implementation of MultiVechicle Simulator(MVS) using
 * MVS controller and Simulation interface.
 */

package MultiVechicleSimulator;

import javax.swing.JFrame;

public class MVSGUIMain extends JFrame {

	private static final long serialVersionUID = 1L;
	private final int width = 800, height = 600;
	private long control_cycle_time = 5; // milliseconds

	private static MVSController mvs_robot_controller;
	private static MVSSimulation mvs_sim;

	/**
	 * Initializes MVS simulation GUI and MVS controller.
	 */
	public MVSGUIMain() {
		mvs_sim = new MVSSimulation();
		add(mvs_sim);

		setTitle("MultiRobotSimulator");
		setSize(width, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setVisible(true);
		setLocationRelativeTo(null);
		setAlwaysOnTop(true);

		mvs_robot_controller = new MVSController(mvs_sim, control_cycle_time);
	}
	
	public static void main(String args[]) {

		MVSGUIMain mvs_gui_sim = new MVSGUIMain();

		System.out.println("MultiVechicleSimulator is ready...");
	}

}
