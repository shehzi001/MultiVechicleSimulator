/**
 * Author : Shehzad Ahmed
 * This Class provides motion controller for each vehicle in the 
 * simulation. It triggers updates in side simulation with updated
 * motion of the activated vehicle.
 */

package MultiVechicleSimulator;


import java.util.Random;

import MultiVechicleAMIImpl.MultiVechicleNode;
import MultiVechicleAMIImpl.MultiVechicleSubscriberImpl;

public class MotionController  extends MultiVechicleSubscriberImpl implements Runnable {
	
	MultiVechicleNode move_command_node, select_command_node;

	private Thread loop;
	private long control_cycle_time = 5; //milliseconds

	private boolean move_forward = false;
	private boolean move_backward = false;
	private int d_theta = 0;
	private int min_angular_vel = 2;
	
	private MVSSimulation mvs;
	private MVSVechicle vechicle;
	private int tmpAngle;
	
	private int vechicle_width = 50;
	private int vechicle_height = 25;
	private int spawn_distance_from_window_boundries = 25;

	
	/**
	 * Constructor initializes communication with UI and starts a thread
	 * which runs at the provided cycle time.
	 * @param mvs Instance of the MVS Simulation class.
	 * @param control_cycle_time Cycle time to compute motion and update
	 * 		  simulation.
	 */
	public MotionController(MVSSimulation mvs, long control_cycle_time) {
		this.mvs = mvs;
		spawnRobot();
		this.control_cycle_time = control_cycle_time;
		
		move_command_node = new MultiVechicleNode();
		move_command_node.init_node("RobotMoveCommandChannel");
		move_command_node.Subscribe("RobotMoveCommand", this);
		move_command_node.Start();

		select_command_node = new MultiVechicleNode();
		select_command_node.init_node("RobotSelectCommandChannel");
		select_command_node.Subscribe("RobotSelectCommand", this);
		select_command_node.Start();

		boolean init_success = move_command_node.is_node_running() & select_command_node.is_node_running();

		if (!init_success) {
			System.out.println(move_command_node.node_name + " : Node is unable to start.");
		}

		loop = new Thread(this);
		loop.start();
	}
	
	/**
	 * Provides implementation of move command callback.
	 */
	@Override
	public void robot_move_command(String msg) {
		moveRobot(msg);
	}
	
	/**
	 * Provides implementation of robot activate command callback.
	 */
	@Override
	public void robot_select_command(int msg) {
		activateRobot(msg);
	}
	
	
	/**
	 * Spawns the robot when the motion controller is initialized.
	 */
	private void spawnRobot() {
		
		Random xrand = new Random();
		int x_Low = spawn_distance_from_window_boundries;
		int x_High = mvs.window_w - spawn_distance_from_window_boundries;
		int x = xrand.nextInt(x_High - x_Low) + x_Low;

		Random yrand = new Random();
		int y_Low = spawn_distance_from_window_boundries;
		int y_High = mvs.window_h - spawn_distance_from_window_boundries;
		int y = yrand.nextInt(y_High - y_Low) + y_Low;
		
		vechicle = new MVSVechicle(x, y, 0, vechicle_width, vechicle_height);
		vechicle.vechicle_id = mvs.spawn_robots(vechicle);
		
		tmpAngle = 0;
		
	}
	
	/**
	 * Sets activation flag of the vehicle.
	 */
	private void activateRobot(int msg) {
		if (vechicle.vechicle_id == (msg-1)) {
			vechicle.vehicle_active = true;
			System.out.print("Vehicle " + msg + " : is activated.");
		} else 
			vechicle.vehicle_active = false;
	}
	
	/**
	 * It configures the parameters for motion computation based on 
	 * on the message content.
	 * @param msg Defines the direction of the robot.
	 */
	private void moveRobot(String msg) {
		if(!vechicle.vehicle_active)
			return;

		if (msg.equals("forward")) {
			move_forward = true;
		} else if (msg.equals("backward")) {
			move_backward = true;
		} else if (msg.equals("left")) {
			d_theta = -min_angular_vel;
		} else if (msg.equals("right")) {
			d_theta = min_angular_vel;
		} else if (msg.equals("forward-right")) {
			move_forward = true;
			d_theta = min_angular_vel;
		} else if (msg.equals("forward-left")) {
			move_forward = true;
			d_theta = -min_angular_vel;
		} else if (msg.equals("backward-left")) {
			move_backward = true;
			d_theta = -min_angular_vel;
		} else if (msg.equals("backward-right")) {
			move_backward = true;
			d_theta = min_angular_vel;
		} else {
			move_forward = false;
			move_backward = false;
			d_theta = 0;
		}
	}
	
	/**
	 * It computes the robot motion using configured parameters.
	 */
	private void updatePosition() {

		if (vechicle.vehicle_active) {
			int v_angle = (tmpAngle + d_theta);

			// this is just to keep the angle between 0 and 360
			if (v_angle > 360) {
				tmpAngle = 0;
			} else if (v_angle < 0) {
				tmpAngle = 360;
			} else {
				tmpAngle = v_angle;
			}

			// setting the Vechicle angle
			vechicle.setA(tmpAngle);

			// moving the Vechicle
			if (move_forward) {
				vechicle.moveForward();
			} else if (move_backward) {
				vechicle.moveBackward();
			}
		}
	}

	/**
	 *  Thread loops after every control cycle time and
	 *  updates the robot motion and simulation.  
	 */
	@Override
	public void run() {

		while (true) {
			updatePosition();
			mvs.updateSimulation(vechicle);
			mvs.updateGUI();
			try {
				loop.sleep(control_cycle_time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.print("Exception vechicle " + vechicle.vechicle_id + " : thread died");
				//e.printStackTrace();
			}

		}

	}
}
