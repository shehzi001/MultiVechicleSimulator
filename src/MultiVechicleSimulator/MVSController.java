/**
 * Author : Shehzad Ahmed
 * This Class provides functionality to spawn vehicles 
 * on the run time using the command sent from the user interface(UI).
 * The controller spawns each vehicle as a sub-motion controller.
 */

package MultiVechicleSimulator;

import java.util.ArrayList;
import java.util.Random;

import MultiVechicleAMIImpl.MultiVechicleNode;
import MultiVechicleAMIImpl.MultiVechicleSubscriberImpl;

public class MVSController extends MultiVechicleSubscriberImpl{
	
	private MultiVechicleNode spawn_command_node;
	
	private MVSSimulation mvs_sim;
	
	private ArrayList<MotionController> mvs_motion_contollers;
	
	private long control_cycle_time = 5; //millisecond
	
	private boolean component_status = false;
	
	/**
	 * Initializes communication with the UI and the buffer to store
	 * instances of the motion controller of each vehicle. 
	 * @param mvs_sim Instance of the MVS Simulation class.
	 * @param control_cycle_time Cycle time to run each motion controller.
	 */
	public MVSController(MVSSimulation mvs_sim, long control_cycle_time) {
		this.mvs_sim = mvs_sim;
		this.control_cycle_time = control_cycle_time;
		
		mvs_motion_contollers = new ArrayList<MotionController>();
		
		mvs_motion_contollers.add(new MotionController(mvs_sim, control_cycle_time));
		
		spawn_command_node = new MultiVechicleNode();
		spawn_command_node.init_node("RobotSpawnCommandChannel");
		spawn_command_node.Subscribe("RobotSpawnCommand", this);
		spawn_command_node.Start();

		boolean init_success = spawn_command_node.is_node_running();

		if (!init_success) {
			System.out.println(spawn_command_node.node_name + " : Node is unable to start.");
			component_status = false;
		} else 
			component_status = true;
	}
	
	/**
	 * Implementation of the spawn command subscriber callback.
	 */
	@Override
	public void robot_spawn_command(int msg) {
		spawnVechicles(msg);
	}
	
	/**
	 * It spawns all the vehicles using motion controller instances.
	 */
	private void spawnVechicles(int msg) {
		System.out.println("spawn vechicles command recieved.");
		
		for(int iter = 0; iter < msg; iter++) {
			mvs_motion_contollers.add(new MotionController(mvs_sim, control_cycle_time));
		}
	}
	
	/**
	 * Simply provides feedback to check if the component status is fine.
	 */
	public boolean isMVSControllerInitialized(int msg) {
		return component_status;
	}

}
