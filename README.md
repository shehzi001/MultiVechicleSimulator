# MultiVechicle Simulator(MVS)
  MultiVechicle Simulator is designed to developed an application with one or more moving vehicles and user interaction. Users can instantiate vehicles, select and manipulate the direction of the 
vehicles. The aim of the simulation is to provide a really nice GUI with vechicles moving in a logistics terminal scenerio.
  
The vechicles are modelled using rectangular boxes and each vechicle represents an independent 
motion controller process. The limitation of the application is that it does not include any
collision checker. Therefore, when the user spawns the vechicles, they might overlap.

## Architecture/design:
   
   MVS-UI system is distributed into multiple components to separate GUI implementation from the communication 
   and control of the vechicles in the simulator. Following are important packages.

    - MultiVechicleAMI: This pacakge is generated using IDL java tool with IDL message file.
    - MultiVechicleAMI.idl : It defines the messages used for Asynchronous Communication Interface(AMI).
    - MultiVechicleAMIImpl : The packages provides implementation of simple asynchronous 
                             communication interface using CORBA library.
    - MultiVechicleUI : Provides a nice user interface to spawn, select and control 
                        robotic vechicles. 
    
    - MultiVechicleSimulator: This packages provides a nice simulator to spawn, select and control 
                        robotic vechicles. The MultiVechicleUI publishes the desired commands to this
                        component using Asynchronous CORBA Communication protocol. 

## Pre-requisites:
	- Java 1.7 or higher
	- Eclipse neon
 	- Common Object Request Broker Architecture (CORBA) communication library.
	- Maven 3.0 or higher

### Maven Installation

	mvn -version
	sudo apt-get purge maven maven2 maven3
	sudo apt-add-repository ppa:andrei-pozolotin/maven3
	sudo apt-get update
	sudo apt-get install maven3

#### CORBA Installation

	git clone https://github.com/JacORB/JacORB

	mvn clean install -DskipTests=true

#### Introduction to CORBA:

	[See this link](https://docs.oracle.com/cd/F49540_01/DOC/java.815/a64683/corba2.htm)

### Create and Compile idl messages:

	idlj  -fall  Message.idl

## MultiVechicle Simulator Usage

### Clone repository and import in eclipse

	git clone https://gitlab.com/shehzi001/MultiVechicleSimulator

	* Import project into eclipse.
	* Update build path to source CORBA eclipse plugin 
	  from lib folder in the repository.
	* Right click on project -> Build Path -> Configure Build Path -> Libraries 
        * Add External JARs(4 jars) from lib folder.
## Run simulator
### Step 1 : start orbd(CORBA) server in another terminal:

	orbd -ORBInitialPort 1050

	* Test Communication using Pub and Sub provided in MultiVechicleUICommTest
          with the simulator.

### Step 2 : Launch MultiVechicleUI from eclipse

### Step 3 : Launch MultiVechicleSimulator from eclipse

	Note : If everything goes well, you will see a UI interface and MVS simulator with initially one vehicle 
	       in the logistic terminal scenerio. Now you can spawn, select and move vechicles.

## Tools used:
	* Java (JEE)
	* Corba based Asynchronous communication between user interface(UI) and MVS.
	* Each vechicle represents a seprate control thread and they continuousy update
  	  the simulation using common simulation resource object.
	* Used AWT/Swing Graphics APIs for GUI developement.

## Future Works
    * Implement feedback from the MVS simulator to UI.
    * Test spawn, select and move vechicles from multiple User interfaces(UI).

